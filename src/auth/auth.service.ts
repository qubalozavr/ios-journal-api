import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { LoginDTO, RegistrationDTO } from 'src/models/user.dto';

@Injectable()
export class AuthService {
  private mockUser = {
    email: 'me@me.com',
    token: 'jwttokenhere',
    username: 'username',
    bio: 'dummy',
    image: null,
  };

  register = async (credentials: RegistrationDTO) => {
    return this.mockUser;
  };

  login = async (credentials: LoginDTO) => {
    if (credentials.email === this.mockUser.email) {
      return this.mockUser;
    } else {
      throw new InternalServerErrorException();
    }
  };
}
