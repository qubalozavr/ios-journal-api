import { Controller, Post, Body, ValidationPipe } from '@nestjs/common';
import { AuthService } from './auth.service';
import { RegistrationDTO } from 'src/models/user.dto';
// import { timingSafeEqual } from 'crypto';

@Controller('users')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post()
  register(@Body(ValidationPipe) credenentials: RegistrationDTO) {
    return this.authService.register(credenentials);
  }

  @Post('/login')
  login(@Body(ValidationPipe) credentials) {
    return this.authService.login(credentials);
  }
}
