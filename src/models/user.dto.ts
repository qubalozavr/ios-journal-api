import { IsEmail, IsString, MinLength, MaxLength } from 'class-validator';

export class RegistrationDTO {
  @IsEmail()
  @IsString()
  email: string;

  @IsString()
  @MinLength(8)
  @MaxLength(32)
  password: string;
}

export class LoginDTO extends RegistrationDTO {
  @IsString()
  username: string;
}
